# Joint Documentation Resources
The content (content being images, text or any other medium contained within this document which is eligible of copyright protection) are jointly copyrighted by Conexxus and IFSF.  All rights are expressly reserved.  

The templates and other resources contained within are to be used by Conexxus or IFSF members when creating documentation for specifications.



